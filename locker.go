package main

import (
	"log"
	"net/http"
	"bytes"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

var (
	masterUIDs []string
	secret string
	accessURL string
)

func main() {
	masterUIDs = strings.Split(os.Getenv("DIHT_LOCK_MASTER_UIDS"), ":")
	secret = os.Getenv("DIHT_LOCK_SECRET")
	accessURL = os.Getenv("DIHT_LOCK_URL")
	if secret == "" || accessURL == "" {
		log.Panicln("DIHT_LOCK_SECRET or DIHT_LOCK_URL not set")
	}

	if (!FindDevices()) {
		log.Panicln("No NFC devices found")
	}
	defer CloseDevices()

	lock, err := NewLock(32)
	if err != nil {
		log.Panicln("Could not initialize lock", err)
	}
	defer lock.Destroy()

	signals := make(chan os.Signal, 2)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	var oldUIDs []string
	for {
		select {
		case <-signals:
			return
		default:
		}

		uids := GetUIDs()
		for _, uid := range uids {
			if !ContainsUID(oldUIDs, uid) && CheckAccess(uid) {
				lock.Unlock()
			}
		}
		oldUIDs = uids
		time.Sleep(100 * time.Millisecond)
	}
}

func ContainsUID(uids []string, targetUID string) bool {
	for _, uid := range(uids) {
		if targetUID == uid {
			return true
		}
	}
	return false
}

func CheckAccess(uid string) bool {
	for _, masterUID := range masterUIDs {
		if uid == masterUID {
			return true
		}
	}

	resp, err := http.PostForm(accessURL + uid + "/", url.Values{"secret": {secret}})
	if err != nil {
		log.Println("Failed to check access", err)
		return false
	}
	defer resp.Body.Close()
	var buf bytes.Buffer
	buf.ReadFrom(resp.Body)
	return buf.String() == "GRANTED"
}
