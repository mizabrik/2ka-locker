package main

import (
	"github.com/fuzxxl/nfc/2.0/nfc"
	"fmt"
	"encoding/hex"
)

var devices []nfc.Device = make([]nfc.Device, 0);

func FindDevices() bool {
	CloseDevices()

	connstrings, _ := nfc.ListDevices()
	for _, connstring := range connstrings {
		dev, err := nfc.Open(connstring)
		if err != nil {
			fmt.Println(err)
		} else {
			devices = append(devices, dev)
		}
	}

	return len(devices) > 0
}

func CloseDevices() {
	for _, dev := range devices {
		dev.Close()
	}

	devices = make([]nfc.Device, 0)
}

func GetUIDs() []string {
	var uids []string

	for _, dev := range devices {
		uids = append(uids, getDeviceUIDs(dev)...)
	}

	return uids
}

func getDeviceUIDs(dev nfc.Device) []string {
	err := dev.InitiatorInit()
	if err != nil {
		fmt.Println(err)
		return nil
	}

	m := nfc.Modulation{Type: nfc.ISO14443a, BaudRate: nfc.Nbr106}
	targets, err := dev.InitiatorListPassiveTargets(m)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	uids := make([]string, 0, len(targets))
	for _, rawTarget := range targets {
		target, ok := rawTarget.(*nfc.ISO14443aTarget)
		if ok {
			uids = append(uids, hex.EncodeToString(target.UID[:target.UIDLen]))
		}
	}

	return uids
}
