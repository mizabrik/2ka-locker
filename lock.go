package main

import (
	"io/ioutil"
	"strconv"
	"time"
)

type Lock struct {
	gpio string
}

func writeFile(file string, s string) error {
	return ioutil.WriteFile(file, []byte(s), 0200)
}

func NewLock(gpioNumber int) (*Lock, error) {
	gpio := strconv.Itoa(gpioNumber)

	err := writeFile("/sys/class/gpio/export", gpio)
	if err != nil {
		return nil, err
	}

	err = writeFile("/sys/class/gpio/gpio" + gpio + "/direction", "out")
	if err != nil {
		return nil, err
	}
	writeFile("/sys/class/gpio/gpio" + gpio + "/value", "0")

	return &Lock{gpio}, nil
}

func (l *Lock) Unlock() {
	valueFile := "/sys/class/gpio/gpio" + l.gpio + "/value"
	writeFile(valueFile, "1")
	time.Sleep(10 * time.Millisecond)
	writeFile(valueFile, "0")
}

func (l *Lock) Destroy() error {
	return writeFile("/sys/class/gpio/unexport", l.gpio)
}
